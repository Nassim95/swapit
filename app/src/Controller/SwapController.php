<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\User;
use App\lib\IgdbBundle\IgdbWrapper\IgdbWrapper;
use App\Entity\Rating;
use App\Entity\Exchange;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SwapController extends AbstractController
{
    /**
     * @Route("/swap_game/{id}/{id_game}", name="swap_game")
     */
    public function index(User $user, Game $id_game, IgdbWrapper $igdb)
    {
        // fetching user games wish
        $userWishes = $user->getWishGames();
        $userWishers = [];
        
        foreach($userWishes as $userWish){
            array_push($userWishers, $userWish);
        }

        $userCurrentOwns = $this->getUser()->getOwnGames();
        $currentUserWishers = [];

        foreach($userCurrentOwns as $userCurrentOwn){
            array_push($currentUserWishers, $userCurrentOwn);
        }

        $matchingGames = [];
        $unmatchingGames = [];

        foreach($userWishers as $userWish){
            foreach($currentUserWishers as $key => $currentUserWisher){
                if($currentUserWisher->getId() == $userWish->getId()){
                    array_push($matchingGames, $currentUserWisher);
                }
            }
        }

        $currentUserGameIds = [];
        foreach($userCurrentOwns as $userCurrentOwn){
            array_push($currentUserGameIds, $userCurrentOwn->getId());
        }

        foreach($userWishes as $userCurrentOwn){
            if(!in_array($userCurrentOwn->getId(), $currentUserGameIds)){
                array_push($unmatchingGames, $userCurrentOwn);
            }
        }

        return $this->render('front/swap/swap_game.html.twig', [
            'userOwnerGame' => $id_game,
            'owner' => $user,
            'matchingGames' => $matchingGames,
            'unmatchingGames' => $unmatchingGames,
            'igdb' => $igdb
        ]);
    }

    /**
     * @Route("/swap_recap/{user}/{selected_game}/{game}/{owner}", name="swap_recap")
     */
    public function swap_recap(User $user, Game $selected_game, Game $game, User $owner, IgdbWrapper $igdb)
    {
        return $this->render('front/swap/swap_recap.html.twig', [
            'user' => $user,
            'owner' => $owner,
            'gameSelected' => $selected_game,
            'game' => $game,
            'currentUser' => $this->getUser(),
            'igdb' => $igdb
        ]);
    }

    /**
     * @Route("/validate_swap/{user}/{selected_game}/{ownerGame}/{owner}", name="validate_swap")
     */
    public function validate_Swap(User $user, Game $selected_game, Game $ownerGame, User $owner, EntityManagerInterface $em, \Swift_Mailer $mailer){
        $exchange = new Exchange();
        
        $exchange->setUserProposer($this->getUser());
        $exchange->setUserOwner($user);
        $exchange->setGame($selected_game);
        $exchange->setOwnerGame($ownerGame);
        
        $em->persist($exchange);
        $em->flush();

        $messageProposerSender = (new \Swift_Message('Votre demande d\'échange a été envoyée !'))
            ->setFrom('swapit.esgi@gmail.com')
            ->setTo($this->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    'mail/sending_swap_proposal.html.twig',
                    [
                        'user' => $this->getUser(),
                        'owner' => $owner,
                        'ownerGame' => $ownerGame,
                        'selected_game' => $selected_game
                    ]
                    ),
                    'text/html'
                );

        $mailer->send($messageProposerSender);

        $messageProposerReceiver = (new \Swift_Message('Vous avez reçu une demande d\'échange !'))
            ->setFrom('swapit.esgi@gmail.com')
            ->setTo($owner->getEmail())
            ->setBody(
                $this->renderView(
                    'mail/receiving_swap_proposal.html.twig',
                    [
                        'exchange' => $exchange,
                        'user' => $this->getUser(),
                        'ownerGame' => $ownerGame,
                        'selected_game' => $selected_game,
                        'owner' => $owner
                    ]
                    ),
                    'text/html'
                );
                
        $mailer->send($messageProposerReceiver);

        return $this->render('front/swap/swap_request_send.html.twig', [
            'user' => $user,
            'owner' => $owner,
            'gameSelected' => $selected_game,
            'currentUser' => $this->getUser()
        ]);
    }

    /**
     * @Route("/rate_user/{owner}", name="rate_user")
     */
    public function rateUser(User $owner, Request $request, EntityManagerInterface $manager){
        $rating = new Rating();
        $ratingValue = $request->query->get('rating');
        
        $rating->setUserId($owner);
        $rating->setRatingValue($ratingValue);

        $manager->persist($rating);
        $manager->flush();

        return $this->render('front/rating/thanks_rating.html.twig');
    }

    /**
     * @Route("/exchange_confirmed/{exchange}", name="exchange_confirmed")
     */
    public function validate_exchange(Exchange $exchange, \Swift_Mailer $mailer, EntityManagerInterface $em){
        if($exchange->getConfirmed() !== null){
            return $this->redirectToRoute('home');
        }

        $exchange->setConfirmed(true);
        $em->persist($exchange);
        $em->flush();
        
        // va rechercher le jeu qui correspond a l'id
        $gameOwner = $this->getDoctrine()->getRepository(Game::class)->find($exchange->getOwnerGame());
        $gameProposer = $this->getDoctrine()->getRepository(Game::class)->find($exchange->getGame()->getId());

        // Ajoute le jeu du Owner dans la liste owngame du proposeur
        $userProposer = $exchange->getUserProposer()->addOwnGame($gameOwner);
        $em->persist($userProposer);
        $em->flush();

        // Ajoute le jeu du proposeur a la liste owngame du owner
        $userOwner = $exchange->getUserOwner()->addOwnGame($gameProposer);
        $em->persist($userOwner);
        $em->flush();

        // retire le jeu echangé de la liste Owngame du proposeur
        $userProposer = $exchange->getUserProposer()->removeOwnGame($gameProposer);
        $em->persist($userProposer);
        $em->flush();

        // retire le jeu echangé de la liste owngame du owner
        $userOwner = $exchange->getUserOwner()->removeOwnGame($gameOwner);
        $em->persist($userOwner);
        $em->flush();

        // retire les jeux souhaités des deux échangeurs
        $userOwner = $exchange->getUserOwner()->removeWishGame($gameProposer);
        $em->persist($userOwner);
        $em->flush();

        $userProposer = $exchange->getUserProposer()->removeWishGame($gameOwner);
        $em->persist($userOwner);
        $em->flush();

        $messageConfirmReceiver = (new \Swift_Message('Votre confirmation de swap a bien été prise en compte !'))
            ->setFrom('swapit.esgi@gmail.com')
            ->setTo($exchange->getUserOwner()->getEmail())
            ->setBody(
                $this->renderView(
                    'mail/swap_confirmation_to_owner.html.twig',
                    [
                        'exchange' => $exchange,
                        'user' => $exchange->getUserProposer(),
                        'ownerGame' => $exchange->getOwnerGame(),
                        'selected_game' => $exchange->getGame(),
                        'owner' => $exchange->getUserOwner()
                    ]
                    ),
                    'text/html'
                );
                
        $mailer->send($messageConfirmReceiver);

        $messageConfirmProposer = (new \Swift_Message('Votre demande de swap a bien été validée !'))
            ->setFrom('swapit.esgi@gmail.com')
            ->setTo($exchange->getUserProposer()->getEmail())
            ->setBody(
                $this->renderView(
                    'mail/swap_confirmation_to_proposer.html.twig',
                    [
                        'exchange' => $exchange,
                        'user' => $exchange->getUserProposer(),
                        'ownerGame' => $exchange->getOwnerGame(),
                        'selected_game' => $exchange->getGame(),
                        'owner' => $exchange->getUserOwner()
                    ]
                    ),
                    'text/html'
                );
                
        $mailer->send($messageConfirmProposer);

        return $this->render('front/swap/swap_confirmed.html.twig');
    }

    /**
     * @Route("/exchange_denied/{exchange}", name="exchange_denied")
     */
    public function exchange_denied(Exchange $exchange, \Swift_Mailer $mailer, EntityManagerInterface $em){
        if($exchange->getConfirmed() !== null){
            return $this->redirectToRoute('home');
        }
        $exchange->setConfirmed(false);
        $em->persist($exchange);
        $em->flush();

        $messageConfirmReceiver = (new \Swift_Message('Votre refus de swap a bien été prise en compte !'))
            ->setFrom('swapit.esgi@gmail.com')
            ->setTo($exchange->getUserOwner()->getEmail())
            ->setBody(
                $this->renderView(
                    'mail/swap_refused_to_owner.html.twig',
                    [
                        'exchange' => $exchange,
                        'user' => $exchange->getUserProposer(),
                        'ownerGame' => $exchange->getOwnerGame(),
                        'selected_game' => $exchange->getGame(),
                        'owner' => $exchange->getUserOwner()
                    ]
                    ),
                    'text/html'
                );
        $mailer->send($messageConfirmReceiver);

        $messageConfirmProposer = (new \Swift_Message('Votre demande de swap a été refusée ! :('))
            ->setFrom('swapit.esgi@gmail.com')
            ->setTo($exchange->getUserProposer()->getEmail())
            ->setBody(
                $this->renderView(
                    'mail/swap_refused_to_proposer.html.twig',
                    [
                        'exchange' => $exchange,
                        'user' => $exchange->getUserProposer(),
                        'ownerGame' => $exchange->getOwnerGame(),
                        'selected_game' => $exchange->getGame(),
                        'owner' => $exchange->getUserOwner()
                    ]
                    ),
                    'text/html'
                );
                
        $mailer->send($messageConfirmProposer);

        return $this->render('front/swap/swap_denied.html.twig');
    }

    
}
