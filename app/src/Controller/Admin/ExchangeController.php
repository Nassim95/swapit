<?php

namespace App\Controller\Admin;

use App\Entity\Exchange;
use App\Repository\ExchangeRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExchangeController extends AbstractController
{
    /**
     * @Route("admin/exchange", name="exchange")
     */
    public function index(ExchangeRepository $exchanges): Response
    {
        return $this->render('admin/exchange/index.html.twig', [
            'exchanges' => $exchanges->findAll()
        ]);
    }


    // A voir avec Andrew, ces deux fonctions sont broken

    // /**
    //  * @Route("/{id}", name="exchange_show", methods={"GET"})
    //  */
    // public function show(Exchange $exchange): Response
    // {
    //     return $this->render('admin/exchange/show.html.twig', [
    //         'exchange' => $exchange
    //     ]);
    // }

    // /**
    //  * @Route("/{id}", name="exchange_delete", methods={"POST"})
    //  */
    // public function delete(Request $request, exchange $exchange): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$exchange->getId(), $request->request->get('_token'))) {
    //         $entityManager = $this->getDoctrine()->getManager();
    //         $entityManager->remove($exchange);
    //         $entityManager->flush();
    //     }

    //     return $this->redirectToRoute('exchange_index');
    // }
}
