<?php

namespace App\lib\IgdbBundle\Mapper;

use App\Entity\Platform;

class PlatformMapper
{
    public static function map(array $input, \Doctrine\ORM\EntityManager $em): ?Platform
    {
        if ($em->getRepository(Platform::class)->find($input['id'])) {

            return null;
        }

        $Platform = new Platform();

        $Platform->setId($input['id']);
        $Platform->setName($input['name']);
        $Platform->setSlug($input['slug'] ?? null);
        $Platform->setUrl($input['url'] ?? null);

        return $Platform;
    }
}
