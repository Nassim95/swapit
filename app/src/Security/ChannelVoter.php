<?php
namespace App\Security;

use App\Entity\Channel;
use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ChannelVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const EDIT = 'edit';

    protected function supports(string $attribute, $subject): bool
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::EDIT])) {
            return false;
        }

        // only vote on `Channel` objects
        if (!$subject instanceof Channel) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        // you know $subject is a Channel object, thanks to `supports()`
        /** @var Channel $Channel */
        $Channel = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($Channel, $user);
            case self::EDIT:
                return $this->canEdit($Channel, $user);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(Channel $Channel, User $user): bool
    {
        // if they can edit, they can view
        if ($this->canEdit($Channel, $user)) {
            return true;
        }

        // the Channel object could have, for example, a method `isPrivate()`
        return ($Channel->getSender() == $user || $Channel->getReceiver() == $user );
    }

    private function canEdit(Channel $Channel, User $user): bool
    {
        // this assumes that the Channel object has a `getOwner()` method
        return ($Channel->getSender() == $user || $Channel->getReceiver() == $user );
    }
}
