<?php

declare(strict_types=1);

namespace App\Services\Mercure;

use App\Entity\Channel;
use Lcobucci\JWT\Configuration;
use Lcobucci\JWT\Signer\Key\LocalFileReference;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key;

class CookieJwtProvider
{
    private Configuration $config;

    public function __construct(string $secret)
    {
        $config = Configuration::forAsymmetricSigner(
            // You may use RSA or ECDSA and all their variations (256, 384, and 512) and EdDSA over Curve25519
            new Sha256(),
            InMemory::plainText($secret),
            InMemory::plainText('Swapit')
            // You may also override the JOSE encoder/decoder if needed by providing extra arguments here
        );
        
        $this->config =$config;
    }

    public function __invoke(Channel $channel): string
    {
     
        
        return ($this->config->builder())
            ->withClaim('mercure', ['subscribe' => [sprintf('http://localhost:8082/channel/%s', $channel->getId())]]) // Attention le claim est différent qu'avec le JWTProvider. Ici on précise le topic privé que l'on souhaite avec le droit "d'accès"
            ->getToken($this->config->signer(), $this->config->signingKey())
            ->toString();
    }
}