<?php 

namespace App\Services;


use Exception;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use App\Entity\Game;

use Symfony\Component\Serializer\Serializer;

class Igdb {

    protected $access_token = null;
    protected $httpClient;
    private $client;
    private $interfaceManager;
    
    private $encoders;
    private $normalizers;
    private $serializer;


    // private $normalizer = new ObjectNormalizer($classMetadataFactory);
    // private $serializer = new Serializer([$normalizer]);
   
    public function __construct($client ,Twitch $twitch, HttpClientInterface $httpClient, EntityManagerInterface $em) {
       
        $this->client = array_pop($client);
        
        if($this->access_token === null) {

            $this->access_token = $twitch->auth()['access_token'];

        }

        $this->httpClient =  $httpClient;
        $this->interfaceManager =  $em;
        $this->encoders = array(new JsonEncoder());
        $this->normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
    }

    public function initCron() {
      
        $this->initCronGames();
    }
    
    public function initCronGames() {

        //  for ($i=0; $i < ($this->countGames()/500); $i++) {  
        //     $offset = ($items == 0)? 0 : $items*500;
        //     $response = $this->getGamesList($offset,500);
        //     $this->serializeDatas($response, Game::class);
        // // }

        // $response = $this->getGenres();
        // $this->serializeDatas($response,Genre::class);

        // $response = $this->getModes();
        // $this->serializeDatas($response,Mode::class);

        for ($i=0; $i < ($this->countGames()/500); $i++) {  
            $offset = ($i == 0)? 0 : $i*500;
            $response = $this->getGamesList($offset,500);
            $this->serializeDatas($response, Game::class);
        }

    }

    public function serializeDatas($datas, $class) {

        foreach($datas as $data) {
            $this->serializeData($data, $class);
        }
    }

    public function serializeData($data, $class) {

            if(!array_key_exists("parent_game", $data))
            {
                foreach ($data as $key => $param) {
                    if (is_array($param)) {
                        dd( $key,$param);
                    }

                }

                $productSerialized = $this->serializer->serialize($data, 'json',['groups' => 'cron']);
            
                $productDeserialized = $this->serializer->deserialize($productSerialized, $class, 'json', ['groups' => 'cron']);
            
                $this->interfaceManager->persist($productDeserialized);
                $this->interfaceManager->flush();
            }  
    }

    public function serelizeDepedencies($product, $id, $class) {
        $class = ucfirst($class);
        $productDependence = new $class();
        $productDependence = $productDependence->getEntityManager()->find($id);
        $product->add.$class($productDependence);
    }

    public function getGamesList($offset, $limit) {

        $response = $this->httpClient->request(
                        'POST','https://api.igdb.com/v4/games',
                        [
                        'headers' => 
                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                        'body' => 'fields name, first_release_date, status, storyline, summary, version_title, age_ratings, aggregated_rating, aggregated_rating_count, follows, genres;
                                    where status = (0,1,5,7);
                                    limit '."$limit;".'
                                    offset '."$offset;",
                        ]
                    )->toArray();
        return $response;
    }

    public function getGameCovers($id) { 
        $response = $this->httpClient->request(
                        'POST','https://api.igdb.com/v4/covers',
                        [
                        'headers' => 
                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                            'body' => 'fields *; where game = '.$id.';'
                        ]
                    )->toArray();
        $new_array = array_reduce($response, 'array_merge', array());
        if(empty($new_array['image_id'])){
            return 'nocover_qhhlj6';
        }
        else{
            return $new_array['image_id'];
        }
    }

    public function countGames() {

        $response = $this->httpClient->request(
                        'POST','https://api.igdb.com/v4/games/count',
                        [
                            'headers' => [
                                    'Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token
                                ],
                            'body' => 'fields name, first_release_date, status, storyline, summary, version_title, age_ratings, parent_game, aggregated_rating, aggregated_rating_count, follows;
                                where status = null;'
                        ]
                    )->toArray();
        dd($response);
        return $response['count'];

    }

    public function countCharacters() {

        $response = $this->httpClient->request(
                        'POST','https://api.igdb.com/v4/games/count',
                        [
                            'headers' => [
                                    'Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token
                                ],
                        ]
                    )->toArray();
        
        return $response['count'];

    }


    public function getGame($id) {

        $response = $this->httpClient->request(
                        'POST','https://api.igdb.com/v4/games',
                        [
                            'headers' => [
                                    'Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token
                                ],
                            'body' => 'fields genres;'." where id = $id ;"
                        ]
                    )->toArray();
        dd($response);
        return array_pop($response);
        
    }

    public function getGenres($id) { 
        $genres = $this->httpClient->request(
                        'POST','https://api.igdb.com/v4/genres',
                        [
                            'headers' => [
                                    'Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token
                                ],
                            'body' => 'fields name, slug;'
                        ]
                    )->toArray();

        $game = $this->httpClient->request(
            'POST','https://api.igdb.com/v4/games',
            [
                'headers' => [
                        'Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token
                    ],
                'body' => 'fields genres;'." where id = $id ;"
            ]
        )->toArray();

        $gameArrayUniq = array_reduce($game, 'array_merge', array());

        if(array_key_exists('genres', $gameArrayUniq) == false){
            return "null";
        }

        $genreNumber = array_shift($gameArrayUniq['genres']);

        for($i=0; $i<count($genres); $i++){
            if($genreNumber == $genres[$i]['id']){
                $gameGenre = $genres[$i]['name'];
                break;
            }
        }

        if(empty($gameGenre)){
            return 'null';
        }else{
            return $gameGenre;
        }                            
    }
    
    public function getCharacters() { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/characters',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields *;'
                                        ]
                                    )->toArray();
        return $response;                            
    }

    public function getGameCharacters($id) { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/characters',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields *;'
                                                ." where games = $id ;"
                                        ]
                                    )->toArray();
        return $response;
    }

    public function getCharacter_mug_shots($id) { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/characters',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields image_id,url;'
                                                ." where games = $id ;"
                                        ]
                                    )->toArray();
        return $response;
    }

    public function getCompanies() { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/companies',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields name, country , description;'
                                        ]
                                    )->toArray(); 
        return $response;
    }

    public function getCompany($id) { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/companies',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields *;'
                                                ." where games = $id ;"
                                        ]
                                    )->toArray(); 
        return $response;
    }

    public function searchGame($search) { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/games',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields *;'
                                                ." search  \"$search\" ;"
                                        ]
                                    )->toArray(); 
        return $response;
    }

    public function getGamesModes() { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/game_modes',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields *;'
                                        ]
                                    )->toArray(); 
        return $response;
    }

    public function getThemes() { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/themes',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields name;'
                                        ]
                                    )->toArray(); 
        return $response;
    }

    public function getGameVideos($id) { 
        $response = $this->httpClient->request('POST','https://api.igdb.com/v4/game_videos',
                                        [
                                        'headers' => 
                                            ['Client-ID' => $this->client, 'Authorization' => 'Bearer '.$this->access_token],
                                        'body' => 'fields *;'
                                                ." where games = $id ;
                                                limit 3;"
                                        ]
                                    )->toArray(); 
        return $response;
    } 
}