<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @Groups({"message"})
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"message"})
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"message"})
     * @Assert\Email()
     */
    private $email;


    /**
     * @ORM\Column(type="json")
     */
    private $roles = ["ROLE_USER"];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity=Offer::class, mappedBy="proposer", orphanRemoval=true)
     */
    private $offers;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, inversedBy="Owners"),
     * @ORM\JoinTable(name="User_Own_Games")
     */
    private $OwnGames;

    /**
     * @ORM\ManyToMany(targetEntity=Game::class, inversedBy="Wishers"),
     * @ORM\JoinTable(name="User_Wish_Games")
     */
    private $WishGames;

    /**
     * @ORM\OneToMany(targetEntity=Exchange::class, mappedBy="UserOwner")
     */
    private $exchanges;

    /**
     * @ORM\OneToMany(targetEntity=Exchange::class, mappedBy="userProposer")
     */
    private $propositions;

    /**
     * @ORM\OneToMany(targetEntity=Rating::class, mappedBy="user")
     */
    private $ratings;
    
    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="author", orphanRemoval=true)
     */
    private $channel;

    /**
     * @ORM\OneToMany(targetEntity=Message::class, mappedBy="author", orphanRemoval=true)
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity=Channel::class, mappedBy="Sender", orphanRemoval=true)
     */
    private $sentChannels;

    /**
     * @ORM\OneToMany(targetEntity=Channel::class, mappedBy="receiver", orphanRemoval=true)
     */
    private $receivedChannels;


    /**
     * @ORM\OneToMany(targetEntity=Comment::class, mappedBy="userComments")
     */
    private $comments;


    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->offers = new ArrayCollection();
        $this->OwnGames = new ArrayCollection();
        $this->WishGames = new ArrayCollection();
        $this->toto = new ArrayCollection();
        $this->exchanges = new ArrayCollection();
        $this->propositions = new ArrayCollection();
        $this->ratings = new ArrayCollection();
        $this->channel = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->sentChannels = new ArrayCollection();
        $this->receivedChannels = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getOwnGames(): Collection
    {
        return $this->OwnGames;
    }

    public function addOwnGame(Game $ownGame): self
    {
        if (!$this->OwnGames->contains($ownGame)) {
            $this->OwnGames[] = $ownGame;
        }

        return $this;
    }

    public function removeOwnGame(Game $ownGame): self
    {
        if ($this->OwnGames->contains($ownGame)) {
            $this->OwnGames->removeElement($ownGame);
        }

        return $this;
    }

    /**
     * @return Collection|Game[]
     */
    public function getWishGames(): Collection
    {
        return $this->WishGames;
    }

    public function addWishGame(Game $wishGame): self
    {
        if (!$this->WishGames->contains($wishGame)) {
            $this->WishGames[] = $wishGame;
        }

        return $this;
    }

    public function removeWishGame(Game $wishGame): self
    {
        if ($this->WishGames->contains($wishGame)) {
            $this->WishGames->removeElement($wishGame);
        }

        return $this;
    }

    /**
     * @return Collection|Offer[]
     */
    public function getOffers(): Collection
    {
        return $this->offers;
    }

    /**
     * Set the value of offers
     *
     * @return  self
     */ 
    public function setOffers($offers)
    {
        $this->offers = $offers;

        return $this;
    }

    public function addOffer(Offer $offer): self
    {
        if (!$this->offers->contains($offer)) {
            $this->offers[] = $offer;
            $offer->setProposer($this);
        }

        return $this;
    }

    public function removeOffer(Offer $offer): self
    {
        if ($this->offers->contains($offer)) {
            $this->offers->removeElement($offer);
            // set the owning side to null (unless already changed)
            if ($offer->getProposer() === $this) {
                $offer->setProposer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exchange[]
     */
    public function getExchanges(): Collection
    {
        return $this->exchanges;
    }

    public function addExchange(Exchange $exchange): self
    {
        if (!$this->exchanges->contains($exchange)) {
            $this->exchanges[] = $exchange;
            $exchange->setUserOwner($this);
        }

        return $this;
    }

    public function removeExchange(Exchange $exchange): self
    {
        if ($this->exchanges->removeElement($exchange)) {
            // set the owning side to null (unless already changed)
            if ($exchange->getUserOwner() === $this) {
                $exchange->setUserOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Exchange[]
     */
    public function getPropositions(): Collection
    {
        return $this->propositions;
    }

    public function addProposition(Exchange $proposition): self
    {
        if (!$this->propositions->contains($proposition)) {
            $this->propositions[] = $proposition;
            $proposition->setUserProposer($this);
        }

        return $this;
    }

    public function removeProposition(Exchange $proposition): self
    {
        if ($this->propositions->removeElement($proposition)) {
            // set the owning side to null (unless already changed)
            if ($proposition->getUserProposer() === $this) {
                $proposition->setUserProposer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Rating[]
     */
    public function getRatings(): Collection
    {
        return $this->ratings;
    }

    public function addRating(Rating $rating): self
    {
        if (!$this->ratings->contains($rating)) {
            $this->ratings[] = $rating;
            $rating->setUserId($this);
        }

        return $this;
    }
    
    /**
     * @return Collection|Message[]
     */
    public function getChannel(): Collection
    {
        return $this->channel;
    }

    public function addChannel(Message $channel): self
    {
        if (!$this->channel->contains($channel)) {
            $this->channel[] = $channel;
            $channel->setAuthor($this);
        }

        return $this;
    }

    public function removeRating(Rating $rating): self
    {
        if ($this->ratings->removeElement($rating)) {
            // set the owning side to null (unless already changed)
            if ($rating->getUserId() === $this) {
                $rating->setUserId(null);
            }
        }

        return $this;
    }

    public function removeChannel(Message $channel): self
    {
        if ($this->channel->removeElement($channel)) {
            // set the owning side to null (unless already changed)
            if ($channel->getAuthor() === $this) {
                $channel->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setAuthor($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getAuthor() === $this) {
                $message->setAuthor(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Channel[]
     */
    public function getSentChannels(): Collection
    {
        return $this->sentChannels;
    }

    public function addSentChannel(Channel $sentChannel): self
    {
        if (!$this->sentChannels->contains($sentChannel)) {
            $this->sentChannels[] = $sentChannel;
            $sentChannel->setSender($this);
        }

        return $this;
    }

    public function removeSentChannel(Channel $sentChannel): self
    {
        if ($this->sentChannels->removeElement($sentChannel)) {
            // set the owning side to null (unless already changed)
            if ($sentChannel->getSender() === $this) {
                $sentChannel->setSender(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Channel[]
     */
    public function getReceivedChannels(): Collection
    {
        return $this->receivedChannels;
    }

    public function addReceivedChannel(Channel $receivedChannel): self
    {
        if (!$this->receivedChannels->contains($receivedChannel)) {
            $this->receivedChannels[] = $receivedChannel;
            $receivedChannel->setReceiver($this);
        }

        return $this;
    }

    public function removeReceivedChannel(Channel $receivedChannel): self
    {
        if ($this->receivedChannels->removeElement($receivedChannel)) {
            // set the owning side to null (unless already changed)
            if ($receivedChannel->getReceiver() === $this) {
                $receivedChannel->setReceiver(null);
            }
        }

        return $this;
    }
    
    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }
}
